'use strict'

var express = require('express');
var server = require('./server.js');
var fs = require("fs");
var sqlite3 = require('sqlite3').verbose();
var file = "test.db";
var db = new sqlite3.Database(file);
var stmt;

module.exports=  {
  createTables: function() {
      var exists = fs.existsSync(file);
      try { //trying to read in the file
        var createDB = fs.readFileSync('db.sql', 'utf8');
      }
      catch (err) { //no SQL file is given
        if (!exists) { //if we don't already have a database with tables in this is a problem
          return 0;
        }
        else return 1; //it is sort of a success as we don't even need to create any tables
      }
      if (!exists) {
        console.log("Creating DB file.");
        fs.openSync(file, "w");
        console.log("Done");
      }
      db.exec(createDB);
      return 1;
    },

    insert: function(values){
      values = values.body;
      stmt = db.prepare("INSERT INTO Students ( FirstName, InsName, Country, StartDate, EndDate) VALUES (?,?,?,?,?)");
      stmt.run(values.first_name,values.insName, values.country, values.start_date,values.end_date);
    },

    deleteTables : function() {
      db.run("DROP TABLE if exists Students");
    },

   getAllStudents: function(callback) {
    var store = { "store": [] };
    var student = {id: "",firstName: "",insName: "",country: "",
      placementStatus: "",startDate: "",endDate: ""};
    db.each("SELECT StudentID FROM Students", function(err,row) {
        getStatus(row.StudentID, function(studentID, placementStatus) {
          student.placementStatus = placementStatus;
          var newObj = JSON.parse(JSON.stringify(student)); //creating copy of student which isn't by reference
          store.store[row.StudentID-1] = newObj;
        });
      },
      function(err,numRows) {
        setTimeout(function(){db.each("SELECT StudentID,FirstName, Country,StartDate, EndDate, InsName FROM Students",
            function(err, row2) {
              store.store[row2.StudentID-1].id = row2.StudentID;
              store.store[row2.StudentID-1].firstName = row2.FirstName;
              store.store[row2.StudentID-1].insName = row2.InsName;
              store.store[row2.StudentID-1].country = row2.Country;
              store.store[row2.StudentID-1].startDate = row2.StartDate;
              store.store[row2.StudentID-1].endDate = row2.EndDate;
            },
            function(err,numRows) {
              store.store = (store.store).filter(Boolean);
              console.log('We got:');
              console.log(store);
              callback(store);
              return store;
        });
    },500)});
  }
}

//Function to return placement status based on where the current date lies in the start and end dates
function getStatus(studentId, callback) {
 stmt = db.prepare("SELECT EXISTS (SELECT 1 FROM Students WHERE (StudentID = ?) AND (date('now') BETWEEN StartDate AND EndDate)) AS x");
 stmt.each(studentId, function(err,res) {
   if (res.x == 1) {//Triggers if current date is between the start and end date
     callback(studentId, "Placement in progress");
     return ("Placement in progress");
   }
 });
 stmt = db.prepare("SELECT EXISTS (SELECT 1 FROM Students WHERE (StudentID = ?) AND (date('now') < StartDate)) AS x");
 stmt.each(studentId, function(err,res) {
   if (res.x == 1) {//Triggers if current date is before the start date
     callback(studentId, "Pre-placement");
     return ("Pre-placement");
   }
 });
 stmt = db.prepare("SELECT EXISTS (SELECT 1 FROM Students WHERE (StudentID = ?) AND (date('now') > EndDate)) AS x");
 stmt.each(studentId, function(err,res) {
   if (res.x == 1) {//Triggers if current date is after the end date
     callback(studentId, "Post-placement");
     return("Post-placement");
   }
 });
}
