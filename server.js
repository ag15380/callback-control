'use strict'

var express = require('express');
var fs = require('fs');
var database = require('./database.js');
var app = express();

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');

  //Starts with a fresh database
  database.deleteTables();
  database.createTables();
  //Inserts test data
  database.insert(testReq);
  database.insert(testReq2);
  database.insert(testReq4);
});

app.get('/:name', function (req, res, next) {
  var options = {
    root: 'html',
  };
  var fileName = req.params.name;
  if (fileName == 'requestStudent') {
    res.setHeader("Access-Control-Allow-Origin","*");
    database.getAllStudents(function(allData){
      res.send(JSON.stringify(allData));
    });
  }
  else {
    res.sendFile(fileName, options);
  }
});

//Add more testReqs if you want
var testReq = {
  body: {
    first_name: 'John',
    insName: 'Stanford University',
    country: 'USA',
    start_date: '2001-06-12',
    end_date: '2002-06-12'
  }
};
var testReq2 = {
  body: {
    first_name: 'Jim',
    insName: 'The france uni',
    country: 'France',
    start_date: '2001-06-12',
    end_date: '2112-06-12'
  }
};
var testReq4 = {
  body: {
    first_name: 'Jack',
    insName: 'Tsinghua University',
    country: 'China',
    start_date: '2123-06-12',
    end_date: '2144-06-12'
  }
};
