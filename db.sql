CREATE TABLE IF NOT EXISTS Students (
  StudentID INTEGER PRIMARY KEY,
  FirstName VARCHAR (255),
  InsName VARCHAR(255),
	Country VARCHAR(255),
	StartDate DATE,
  EndDate DATE
);
